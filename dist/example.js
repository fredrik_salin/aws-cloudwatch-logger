'use strict';

var _index = require('./index');

var Logger = (0, _index.initLogger)('serviceName');

Logger.log('info', 'Some message here...', { tags: 'startup,mongo' });