'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loggerMiddleware = exports.initLogger = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _winston = require('winston');

var _winston2 = _interopRequireDefault(_winston);

var _winstonAwsCloudwatch = require('winston-aws-cloudwatch');

var _winstonAwsCloudwatch2 = _interopRequireDefault(_winstonAwsCloudwatch);

var _expressWinston = require('express-winston');

var _expressWinston2 = _interopRequireDefault(_expressWinston);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NODE_ENV = process.env.NODE_ENV || 'development';

var initLogger = function initLogger(serviceName) {
    var useCustomFormatter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;


    var serviceMetaData = {
        service: serviceName
    };

    var WinstonTransport = process.env.NODE_ENV === 'test' ? _winston2.default.transports.Memory : _winston2.default.transports.Console;

    var Logger = new _winston2.default.Logger({
        transports: [new WinstonTransport({
            timestamp: true,
            colorize: true
        })]
    });

    var config = {
        logGroupName: 'udachef-microservices',
        logStreamName: NODE_ENV,
        createLogGroup: false,
        createLogStream: true,
        awsConfig: {
            accessKeyId: process.env.CLOUDWATCH_ACCESS_KEY_ID,
            secretAccessKey: process.env.CLOUDWATCH_SECRET_ACCESS_KEY,
            region: process.env.CLOUDWATCH_REGION
        }
    };

    if (useCustomFormatter) {
        config.formatLog = function (item) {
            return item.level + ': ' + item.message + ' ' + JSON.stringify(_extends({}, serviceMetaData, item.meta));
        };
    }

    if (NODE_ENV === 'production') Logger.add(_winstonAwsCloudwatch2.default, config);

    Logger.level = process.env.LOG_LEVEL || "silly";

    Logger.on('error', function (err) {
        console.log(err);
    });

    return Logger;
};

var loggerMiddleware = function loggerMiddleware(serviceName) {
    _expressWinston2.default.requestWhitelist.push('body');
    _expressWinston2.default.responseWhitelist.push('body');

    return _expressWinston2.default.logger({
        winstonInstance: initLogger('push-service', false),
        msg: serviceName + ' HTTP {{req.method}} {{req.url}}'
    });
};

exports.initLogger = initLogger;
exports.loggerMiddleware = loggerMiddleware;