import winston from 'winston'
import CloudWatchTransport from 'winston-aws-cloudwatch'
import expressWinston from 'express-winston'

const NODE_ENV = process.env.NODE_ENV || 'development'

const initLogger = (serviceName, useCustomFormatter = true) => {

    const serviceMetaData = {
        service: serviceName,
    }

    const WinstonTransport =
        !process.env.SHOW_LOG || process.env.NODE_ENV === 'test' ? winston.transports.Memory : winston.transports.Console

    const Logger = new winston.Logger({
        transports: [
            new WinstonTransport({
                timestamp: true,
                colorize: true,
            })
        ]
    })

    const config = {
        logGroupName: 'udachef-microservices',
        logStreamName: NODE_ENV,
        createLogGroup: false,
        createLogStream: true,
        awsConfig: {
            accessKeyId: process.env.CLOUDWATCH_ACCESS_KEY_ID,
            secretAccessKey: process.env.CLOUDWATCH_SECRET_ACCESS_KEY,
            region: process.env.CLOUDWATCH_REGION,
        },
    }

    if (useCustomFormatter) {
        config.formatLog = item => {
            return `${item.level}: ${item.message} ${JSON.stringify({...serviceMetaData, ...item.meta})}`
        }
    }

    if (NODE_ENV === 'production') Logger.add(CloudWatchTransport, config)

    Logger.level = process.env.LOG_LEVEL || "silly"

    Logger.on('error',  err => { console.log(err) })

    return Logger

}

const loggerMiddleware = serviceName => {
    expressWinston.requestWhitelist.push('body')
    expressWinston.responseWhitelist.push('body')

    return expressWinston.logger({
        winstonInstance: initLogger(serviceName, false),
        msg: `${serviceName} HTTP {{req.method}} {{req.url}}`,
    })
}

export {
    initLogger,
    loggerMiddleware,
}