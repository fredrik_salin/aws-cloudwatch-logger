import {initLogger} from './index'

const Logger = initLogger('serviceName')

Logger.log('info', 'Some message here...', {tags: 'startup,mongo'})
